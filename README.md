# ignisbot_3Dprints
Get all the assembly in OnShape online CAD platform .

[IgnisBot OnShape Full Assembly](https://cad.onshape.com/documents/c03c0a46658c5c297d778863/w/e91492e8fea813cc0aaf558c/e/1ec7413561701ae68585e6db)

There you can edit them and change them as you please. You can also export them in different formats.

Here you will find the latest exported STL files in meters.



![Render STL files](3DPrintFiles/render.png "Render 3Dprintable STL files")

